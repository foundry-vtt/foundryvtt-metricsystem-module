[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&prefix=v&query=version&url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt%2Ffoundryvtt-metricsystem-module%2F-%2Fraw%2Fmaster%2Fmodule.json)](https://gitlab.com/foundry-vtt/foundryvtt-metricsystem-module) [![Min Core](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Min%20Core&prefix=v&query=minimumCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt%2Ffoundryvtt-metricsystem-module%2F-%2Fraw%2Fmaster%2Fmodule.json)](http://foundryvtt.com/) [![Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt%2Ffoundryvtt-metricsystem-module%2F-%2Fraw%2Fmaster%2Fmodule.json)](http://foundryvtt.com/) [![DnD 5e Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=5e%20Compatible&prefix=v&query=minimumSystemVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt%2Ffoundryvtt-metricsystem-module%2F-%2Fraw%2Fmaster%2Fmodule.json)](https://foundryvtt.com/packages/dnd5e/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&logoColor=white)](https://discordapp.com/invite/DDBZUDf)

# FoundryVTT Metric System

## Português

Esse módulo adiciona opções para se jogar D&D 5e no FoundryVTT usando o sistema métrico. 

### Funcionalidades
* Adiciona "metros" e "kilometros" como opção para Alcance (Range) e Alvo (Target) nos itens, habilidades e magias. Também adiciona como opção para definir Movimento/Deslocamento dos atores.  
* Novas cenas automaticamente são padronizadas com a medição no sistema métrico (cada quadrado do grid é 1.5m).  
* Novos atores automaticamente são padronizados com deslocamento de caminhada de 9m e visão noturna de 9m.  
* Jogadores com a permissão GM ou GM Assistente tem acesso a um conversor de medidas no topo da ficha de qualquer ator. Esse conversor é capaz de converter os valores e unidades de medida de itens, habilidades e magias do ator. (Pode ser desabilitado nas configurações do módulo).  
* Muda os rótulos de peso "lbs." para "kgs" e ajusta o cálculo da capacidade de carga dos atores para o sistema métrico, utilizando quilogramas. (Desativado por padrão. Deve ser habilitado nas configurações do módulo. Também habilita um conversor de pesos da mesma maneira que o conversor de medidas)


> Atualizado para funcionar com a versão 0.7.9 do FoundryVTT e com a versão 1.24 do sistema 5th Edition.

### Instalação

O módulo está disponível na lista de Módulos Complementares para instalar com o nome de `Metric System for D&D5e`.

#### Instalação por Manifesto

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/foundry-vtt/foundryvtt-metricsystem-module/raw/master/module.json`

#### Instalação Manual

Se as opções acima não funcionarem, faça o download do arquivo [metric-system-dnd5e.zip](https://gitlab.com/foundry-vtt/foundryvtt-metricsystem-module/-/jobs/artifacts/master/raw/metric-system-dnd5e.zip?job=build "metric-system-dnd5e.zip") e extraia o conteúdo dele dentro da pasta `Data/modules/`

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo.


---


## English

This module adds options to play DnD 5e on FoundryVTT using the metric system.

### Features
* Adds "meters" and "kilometers" as options for Range and Target units on items, features and spells. Also adds "meters" and "kilometers" as options to define actor Movement/Speeds.
* New scenes are automatically set with measurements in the metric system (each square in the grid represents 1.5m)  
* New actors are automatically set with movement speed equal to 9m and dim sight of 9m.
* Players with GM ou Assistant GM roles have access to a measurement conversor at the top of any actor sheet. This conversor is capable to convert the values and units of items, features and spells of the actor. (Can be disabled in the module settings)
* Changes the label "lbs." to "kgs" and adjusts the encumbrance calculation of actors to the metric system, using kilograms. (Disabled by default. Must be enabled in the module settings. Also enables a weight conversor in the same way as the measurement conversor)

> Updated to work with version 0.7.9 of FoundryVTT and with version 1.24 of the 5th Edition System.

### Installation

The module is available in the Add-on Modules list to install with the name `Metric System for D&D5e`.

#### Manifest Installation

In the `Add-On Modules` option click on `Install Module` and place the following link in the field `Manifest URL`

`https://gitlab.com/foundry-vtt/foundryvtt-metricsystem-module/raw/master/module.json`

#### Manual Installation

If the above options do not work, download the [metric-system-dnd5e.zip](https://gitlab.com/foundry-vtt/foundryvtt-metricsystem-module/-/jobs/artifacts/master/raw/metric-system-dnd5e.zip?job=build "metric-system-dnd5e.zip") file and extract its contents into the `Data/modules/` folder

Once this is done, enable the module in the settings of the world in which you intend to use it.

